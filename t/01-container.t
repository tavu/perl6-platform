use v6;
use lib 'lib';
use lib '../lib';
use Test;
use App::Platform::Container;
use JSON::Tiny;

plan 4;

subtest 'bridge network found' => sub {
	plan 1;

    my $proc = run <docker network inspect notfound>, :out, :err;
    my $out = $proc.out.slurp-rest(:close);
	my $result = from-json $out;

	ok($result.elems <= 0, 'network');
}

subtest 'notexists network not found' => sub {
	plan 1;

    my $proc = run <docker network inspect bridge>, :out, :err;
    my $out = $proc.out.slurp-rest(:close);
	my $result = from-json $out;

	ok($result.elems > 0, 'network');
}

subtest 'not exists' => sub {
	plan 1;

	my $test = App::Platform::Container.new(
		:data-path('/tmp'),
		:network('notexists')
	);

	ok(! $test.network-exists, 'network');
}

subtest 'exists' => sub {
	plan 1;

	my $test = App::Platform::Container.new(
		:data-path('/tmp'),
		:network('bridge')
	);

	ok($test.network-exists, 'network');
}

done-testing;
